package cases_test

import (
	"testing"

	"gitlab.com/arch-mage/cases"
)

func TestKebab(t *testing.T) {
	tests := []struct {
		source string
		expect string
	}{
		{"A", "a"},
		{"ID", "id"},
		{"UserID", "user-id"},
		{"userID", "user-id"},
		{"User_ID", "user-id"},
		{"User ID", "user-id"},
		{"IDUser", "id-user"},
		{"num0", "num-0"},
		{"0num", "0-num"},
		{"num0num", "num-0-num"},
		{"0num0", "0-num-0"},
		{"00num00", "00-num-00"},
		{"NUM0", "num-0"},
		{"0NUM", "0-num"},
		{"NUM0NUM", "num-0-num"},
		{"0NUM0", "0-num-0"},
		{"00NUM00", "00-num-00"},
	}

	for _, test := range tests {
		if actual := cases.Kebab(test.source); test.expect != actual {
			t.Errorf("cases.Kebab(%q) should be %q but got %q", test.source, test.expect, actual)
		}
	}
}

func TestSnake(t *testing.T) {
	tests := []struct {
		source string
		expect string
	}{
		{"A", "a"},
		{"ID", "id"},
		{"UserID", "user_id"},
		{"userID", "user_id"},
		{"User_ID", "user_id"},
		{"User ID", "user_id"},
		{"IDUser", "id_user"},
		{"num0", "num_0"},
		{"0num", "0_num"},
		{"num0num", "num_0_num"},
		{"0num0", "0_num_0"},
		{"00num00", "00_num_00"},
		{"NUM0", "num_0"},
		{"0NUM", "0_num"},
		{"NUM0NUM", "num_0_num"},
		{"0NUM0", "0_num_0"},
		{"00NUM00", "00_num_00"},
	}

	for _, test := range tests {
		if actual := cases.Snake(test.source); test.expect != actual {
			t.Errorf("cases.Snake(%q) should be %q but got %q", test.source, test.expect, actual)
		}
	}
}

func TestCamel(t *testing.T) {
	tests := []struct {
		source string
		expect string
	}{
		{"", ""},
		{" ", ""},
		{"\t", ""},
		{"      case", "case"},
		{"space case", "spaceCase"},
		{"tab\tcase", "tabCase"},
		{"kebab-case", "kebabCase"},
		{"snake_case", "snakeCase"},
		{"dotted.case", "dottedCase"},
		{"UPPER", "UPPER"},
		{"0number0", "0number0"},
	}

	for _, test := range tests {
		if actual := cases.Camel(test.source); test.expect != actual {
			t.Errorf("cases.Camel(%q) should be %q but got %q", test.source, test.expect, actual)
		}
	}
}
