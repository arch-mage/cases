// Package cases provides some case conversion functions.
package cases

import "strings"

func cases(str string, sep byte) string {
	str = strings.Trim(str, " ")
	var buff strings.Builder
	buff.Grow(len(str))

	n := len(str)

	for i := 0; i < n; i++ {
		ch := str[i]

		var nextChanged bool
		var next byte

		if i+1 < n {
			next = str[i+1]
			switch {
			case ch >= 'A' && ch <= 'Z' && next >= 'a' && next <= 'z':
				nextChanged = true
			case ch >= 'A' && ch <= 'Z' && next >= '0' && next <= '9':
				nextChanged = true
			case ch >= 'a' && ch <= 'z' && next >= 'A' && next <= 'Z':
				nextChanged = true
			case ch >= 'a' && ch <= 'z' && next >= '0' && next <= '9':
				nextChanged = true
			case ch >= '0' && ch <= '9' && next >= 'A' && next <= 'Z':
				nextChanged = true
			case ch >= '0' && ch <= '9' && next >= 'a' && next <= 'z':
				nextChanged = true
			}
		}

		if i > 0 && buff.String()[buff.Len()-1] != sep && nextChanged {
			if ch >= 'A' && ch <= 'Z' {
				if next >= 'a' && next <= 'z' {
					buff.WriteByte(sep)
					buff.WriteByte(ch + 32)
				} else {
					buff.WriteByte(ch + 32)
					buff.WriteByte(sep)
				}
			} else if ch >= 'a' && ch <= 'z' {
				buff.WriteByte(ch)
				buff.WriteByte(sep)
			} else if ch >= '0' && ch <= '9' {
				buff.WriteByte(ch)
				buff.WriteByte(sep)
			}
		} else if nextChanged && ch >= '0' && ch <= '9' {
			buff.WriteByte(ch)
			buff.WriteByte(sep)
		} else if ch == ' ' || ch == '-' || ch == '_' {
			buff.WriteByte(sep)
		} else if ch >= 'A' && ch <= 'Z' {
			buff.WriteByte(ch + 32)
		} else {
			buff.WriteByte(ch)
		}
	}

	return buff.String()
}

// Kebab converts str to kebab cases.
func Kebab(str string) string {
	return cases(str, '-')
}

// Snake converts str to snake cases.
func Snake(str string) string {
	return cases(str, '_')
}

// Camel converts str to camel cases.
func Camel(str string) string {
	n := len(str)

	if n == 0 {
		return str
	}

	var i int
	for i < n && str[i] == ' ' {
		i++
	}

	if i >= n {
		return ""
	}

	var buff strings.Builder
	buff.Grow(n)

	var upper bool
	for ; i < n; i++ {
		ch := str[i]

		switch {
		case ch >= '0' && ch <= '9':
			buff.WriteByte(ch)
		case ch >= 'A' && ch <= 'Z':
			buff.WriteByte(ch)
		case ch >= 'a' && ch <= 'z':
			if upper {
				ch -= 32
			}
			buff.WriteByte(ch)
		}

		switch ch {
		case ' ', '\t', '-', '_', '.':
			upper = true
		default:
			upper = false
		}
	}

	return buff.String()
}
